package fun.madeby.models;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name = "brewers")
public class Brewer implements Serializable {
@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
@Column(name = "Id")
private long id;
@Column(name = "Name")
private String name;
@Column(name = "Address")
private String address;
@Column(name = "ZipCode")
private String zipCode;
@Column(name = "City")
private String city;
@Column(name = "Turnover")
private int turnover;
private int beers;

// Removed for now for simplicity and for step by stem implementation This info does not exist in the
// brewers table, it is obtained and set from BrewerId foreign key in the Beers table
/*@OneToMany(mappedBy = "brewer", cascade = {CascadeType.PERSIST, CascadeType.PERSIST, CascadeType.DETACH, CascadeType.REFRESH})
private Set<Beer> beers = new HashSet<>();*/

public Brewer() {
}

public Brewer(long id, String name, String address, String zipCode, String city, int turnover, int beers) {
	this.id = id;
	this.name = name;
	this.address = address;
	this.zipCode = zipCode;
	this.city = city;
	this.turnover = turnover;
	this.beers = beers;
}

// Temporary constructor


// This constructor returns itself but with a beer HashSet
/*public Brewer(String name, String address, String zipCode, String city, int turnover) {
	this(name, address, zipCode, city, turnover, new HashSet<>());
}*/

// A constructor to set everything barring id
/*public Brewer(String name, String address, String zipCode, String city, int turnover, Set<Beer> beers) {
	setName(name);
	setAddress(address);
	setZipCode(zipCode);
	setCity(city);
	setTurnover(turnover);
	setBeers(beers);
}*/

public long getId() {
	return id;
}

public void setId(long id) {
	this.id = id;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public String getAddress() {
	return address;
}

public void setAddress(String address) {
	this.address = address;
}

public String getZipCode() {
	return zipCode;
}

public void setZipCode(String zipCode) {
	this.zipCode = zipCode;
}

public String getCity() {
	return city;
}

public void setCity(String city) {
	this.city = city;
}

public int getTurnover() {
	return turnover;
}

public void setTurnover(int turnover) {
	this.turnover = turnover;
}

public int getBeers() {
	return beers;
}

public void setBeers(int beers) {
	this.beers = beers;
}

/*@Override //nice understanding of Beers and how to print them out by brewer.
public String toString() {
	return String.format("%d: %s%nAddress: %s, %s %s%nTurnover: %d%nBrews: %s%n",
		getId(),
		getName(),
		getAddress(),
		getZipCode(),
		getCity(),
		getTurnover(),
		getBeers().stream().map(Beer::getName).collect(Collectors.toList())
	);
}*/

}