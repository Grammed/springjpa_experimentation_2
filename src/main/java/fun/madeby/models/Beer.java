package fun.madeby.models;
import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "beers")
public class Beer {
@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
@Column(name = "Id")
private int id;
@Version
@Column(name = "Version")
private int version;
@Column(name = "Name")
private String name;
@Column(name = "BrewerId")
private int brewer;
@Column(name = "CategoryId")
private int category;
@Column(name = "Price")
private double price;
@Column(name = "Stock")
private int stock;
@Column(name = "Alcohol")
private float alcohol;

public Beer() {}

public Beer(int id, int version, String name, int brewer, int category, double price, int stock, float alcohol) {
	this.id = id;
	this.version = version;
	this.name = name;
	this.brewer = brewer;
	this.category = category;
	this.price = price;
	this.stock = stock;
	this.alcohol = alcohol;
}

public int getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}

public int getVersion() {
	return version;
}

public void setVersion(int version) {
	this.version = version;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public int getBrewer() {
	return brewer;
}

public void setBrewer(int brewer) {
	this.brewer = brewer;
}

public int getCategory() {
	return category;
}

public void setCategory(int category) {
	this.category = category;
}

public double getPrice() {
	return price;
}

public void setPrice(double price) {
	this.price = price;
}

public int getStock() {
	return stock;
}

public void setStock(int stock) {
	this.stock = stock;
}

public float getAlcohol() {
	return alcohol;
}

public void setAlcohol(float alcohol) {
	this.alcohol = alcohol;
}

@Override
public String toString() {
	return "Beer{" +
		       "id=" + id +
		       ", version=" + version +
		       ", name='" + name + '\'' +
		       ", brewer=" + brewer +
		       ", category=" + category +
		       ", price=" + price +
		       ", stock=" + stock +
		       ", alcohol=" + alcohol +
		       '}';
}

@Override
public boolean equals(Object o) {
	if (this == o) return true;
	if (o == null || getClass() != o.getClass()) return false;
	Beer beer = (Beer) o;
	return id == beer.id;
}

@Override
public int hashCode() {
	return Objects.hash(id);
}
}