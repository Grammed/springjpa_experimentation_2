package fun.madeby.services;

import fun.madeby.models.Beer;
import fun.madeby.repositories.BeerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BeerServiceImpl implements BeerService {
@Autowired
private BeerRepository beerRepository;


@Override
public List<Beer> findAll() {
	return beerRepository.findAll();
}
@Override
public Beer save(Beer beer) {
	beerRepository.save(beer);
	return beer;
}
@Override
public Beer findById(Integer id) {
	if(beerRepository.findById(id).isPresent())
		return beerRepository.findById(id).get();
	return null;
}
@Override
public void delete(Integer id) {
	Beer beer  = findById(id);
	beerRepository.delete(beer);
}


}