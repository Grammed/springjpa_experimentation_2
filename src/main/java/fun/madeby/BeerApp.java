package fun.madeby;

import fun.madeby.services.BeerServiceImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class BeerApp {

public static void main(String[] args) {
	ConfigurableApplicationContext ctx = SpringApplication.run(BeerApp.class);
	BeerServiceImpl beerServiceImpl = ctx.getBean(BeerServiceImpl.class);


	System.out.println(beerServiceImpl.findById(5).toString());

}
}
